package com.bringsolutions.autolavadomovil.beta.objetos;

public class OrdenServicio {
    private String ord_clave;
    private String estatus;
    private String sol_direccion;
    private String sol_responsable;
    private String sol_contacto;

    public OrdenServicio(String ord_clave, String estatus, String sol_direccion, String sol_responsable, String sol_contacto) {
        this.ord_clave = ord_clave;
        this.estatus = estatus;
        this.sol_direccion = sol_direccion;
        this.sol_responsable = sol_responsable;
        this.sol_contacto = sol_contacto;
    }

    public String getOrd_clave() {
        return ord_clave;
    }

    public void setOrd_clave(String ord_clave) {
        this.ord_clave = ord_clave;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getSol_direccion() {
        return sol_direccion;
    }

    public void setSol_direccion(String sol_direccion) {
        this.sol_direccion = sol_direccion;
    }

    public String getSol_responsable() {
        return sol_responsable;
    }

    public void setSol_responsable(String sol_responsable) {
        this.sol_responsable = sol_responsable;
    }

    public String getSol_contacto() {
        return sol_contacto;
    }

    public void setSol_contacto(String sol_contacto) {
        this.sol_contacto = sol_contacto;
    }
}
