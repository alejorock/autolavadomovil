package com.bringsolutions.autolavadomovil.beta.adaptadores.admin;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.autolavadomovil.R;
import com.bringsolutions.autolavadomovil.beta.objetos.Constantes;
import com.bringsolutions.autolavadomovil.beta.objetos.TipoServicio;

import org.json.JSONObject;

import java.util.List;

public class TiposServiciosAdapter extends RecyclerView.Adapter<TiposServiciosAdapter.ViewHolder> {
    AlertDialog dialog;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNombre, tvPrecio;
        private CardView tarjeta;
        private Button btnActualizarInformacion, btnEliminarServicio;

        public ViewHolder(View itemView) {
            super(itemView);

            tvNombre = itemView.findViewById(R.id.tvNombreServicio);
            tvPrecio = itemView.findViewById(R.id.tvNombrePrecioServicio);
            tarjeta = itemView.findViewById(R.id.tarjetaTipoServicio);
            btnActualizarInformacion = itemView.findViewById(R.id.btnModificarInformacionServicio);
            btnEliminarServicio = itemView.findViewById(R.id.btnEliminarServicio);

        }
    }

    public List<TipoServicio> tipoServicioList;
    public Context context;

    public TiposServiciosAdapter(List<TipoServicio> tipoServicioList, Context context) {
        this.tipoServicioList = tipoServicioList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_tipo_servicio, viewGroup, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.tvNombre.setText(tipoServicioList.get(i).getTipo_servicio());
        viewHolder.tvPrecio.setText(tipoServicioList.get(i).getTipo_ser_costo());

        viewHolder.btnActualizarInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lanzarDialogoModificacionInformacion(tipoServicioList.get(i), i, viewHolder);

            }
        });

        viewHolder.btnEliminarServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    private void lanzarDialogoModificacionInformacion(final TipoServicio tipoServicio, final int i, final ViewHolder viewHolder) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewDialog = layoutInflater.inflate(R.layout.dialogo_modificar_informacion_servicio, null);

        builder.setView(viewDialog);


        final EditText cajaNombre, cajaCosto;
        final Button btnActualizarInformacion;

        cajaNombre = viewDialog.findViewById(R.id.cajaNombreServicio);
        cajaCosto = viewDialog.findViewById(R.id.cajaCostoServicio);
        btnActualizarInformacion = viewDialog.findViewById(R.id.btnGuardarDatosServicio);

        cajaNombre.setText(tipoServicio.getTipo_servicio());
        cajaCosto.setText(tipoServicio.getTipo_ser_costo());

        btnActualizarInformacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualizarInformacionServicio(cajaNombre.getText().toString(), cajaCosto.getText().toString(), tipoServicio.getTipo_ser_id(), i, viewHolder);

            }
        });

        dialog = builder.create();

        dialog.show();

    }

    private void actualizarInformacionServicio(final String nombre, final String costo, String id, final int pos, final ViewHolder viewHolder) {

        final RequestQueue requestQueue = Volley.newRequestQueue(context);

        String url = Constantes.URL_PRINCIPAL + "actualizar_info_tipo_servicio.php?nombre=" + nombre + "&precio=" + costo + "&id=" + id;

        url = url.replace(" ", "%20");
        StringRequest stringRequest = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                viewHolder.tvNombre.setText(nombre);
                viewHolder.tvPrecio.setText(costo);
                tipoServicioList.get(pos).setTipo_servicio(nombre);
                tipoServicioList.get(pos).setTipo_ser_costo(costo);

                Toast.makeText(context, "¡Servicio actualizado con éxito!", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Se produjo un problema a la hora de actualizar la información.", Toast.LENGTH_SHORT).show();

                dialog.dismiss();

            }
        });

        requestQueue.add(stringRequest);


    }

    public int getItemCount() {
        return tipoServicioList.size();
    }

}
