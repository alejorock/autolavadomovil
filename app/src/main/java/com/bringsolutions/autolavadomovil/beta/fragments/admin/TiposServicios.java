package com.bringsolutions.autolavadomovil.beta.fragments.admin;


import android.os.Bundle;

import android.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.autolavadomovil.R;
import com.bringsolutions.autolavadomovil.beta.adaptadores.admin.TiposServiciosAdapter;
import com.bringsolutions.autolavadomovil.beta.objetos.Constantes;
import com.bringsolutions.autolavadomovil.beta.objetos.TipoServicio;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TiposServicios extends Fragment {

    View view;
    RecyclerView recyclerTiposServicios;
    private TiposServiciosAdapter tiposServiciosAdapter;
    List<TipoServicio> TIPOS_SERVICIOS_LISTA = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tipos_servicios, container, false);

        recyclerTiposServicios = view.findViewById(R.id.recyclerTiposServicio);
        recyclerTiposServicios.setLayoutManager(new LinearLayoutManager(getActivity()));
        poblarRecyler();

        return view;
    }

    private void poblarRecyler() {
        TIPOS_SERVICIOS_LISTA.clear();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        String url = Constantes.URL_PRINCIPAL+"obtener_tipos_servicios.php";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray arrayCapacitaciones = response.getJSONArray("tipos_servicios");
                    JSONObject jsonObject = null;

                    for (int i = 0; i < arrayCapacitaciones.length(); i++) {
                        jsonObject = arrayCapacitaciones.getJSONObject(i);


                        String tipo_ser_id = jsonObject.optString("tipo_ser_id");
                        String tipo_servicio = jsonObject.optString("tipo_servicio");
                        String tipo_ser_costo = jsonObject.optString("tipo_ser_costo");

                        TIPOS_SERVICIOS_LISTA.add(new TipoServicio(tipo_ser_id, tipo_servicio, tipo_ser_costo));

                        tiposServiciosAdapter = new TiposServiciosAdapter(TIPOS_SERVICIOS_LISTA, getActivity());
                        recyclerTiposServicios.setAdapter(tiposServiciosAdapter);
                        tiposServiciosAdapter.notifyDataSetChanged();


                    }


                } catch (Exception e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override


            public void onErrorResponse(VolleyError error) {
                System.out.println("EL error es: " + error.toString());

            }


        });

        requestQueue.add(jsonObjectRequest);


    }


}
