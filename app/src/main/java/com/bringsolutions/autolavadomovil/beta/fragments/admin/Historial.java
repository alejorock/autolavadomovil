package com.bringsolutions.autolavadomovil.beta.fragments.admin;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bringsolutions.autolavadomovil.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Historial extends Fragment {


    public Historial() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_historial, container, false);
    }

}
