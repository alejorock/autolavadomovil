package com.bringsolutions.autolavadomovil.beta.objetos;

import java.io.Serializable;

public class ServicioOrdenClave implements Serializable {
    private String sol_responsable;
    private String sol_direccion;
    private String sol_contacto;
    private String tipo_servicio;
    private String tipo_carro;
    private String tipo_ser_costo;
    private String estatus;

    public ServicioOrdenClave(String tipo_servicio, String tipo_carro, String tipo_ser_costo) {
        this.tipo_servicio = tipo_servicio;
        this.tipo_carro = tipo_carro;
        this.tipo_ser_costo = tipo_ser_costo;
    }

    public ServicioOrdenClave(String sol_responsable, String sol_direccion, String sol_contacto, String tipo_servicio, String tipo_carro, String tipo_ser_costo, String estatus) {
        this.sol_responsable = sol_responsable;
        this.sol_direccion = sol_direccion;
        this.sol_contacto = sol_contacto;
        this.tipo_servicio = tipo_servicio;
        this.tipo_carro = tipo_carro;
        this.tipo_ser_costo = tipo_ser_costo;
        this.estatus = estatus;
    }

    public String getSol_responsable() {
        return sol_responsable;
    }

    public void setSol_responsable(String sol_responsable) {
        this.sol_responsable = sol_responsable;
    }

    public String getSol_direccion() {
        return sol_direccion;
    }

    public void setSol_direccion(String sol_direccion) {
        this.sol_direccion = sol_direccion;
    }

    public String getSol_contacto() {
        return sol_contacto;
    }

    public void setSol_contacto(String sol_contacto) {
        this.sol_contacto = sol_contacto;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getTipo_servicio() {
        return tipo_servicio;
    }

    public void setTipo_servicio(String tipo_servicio) {
        this.tipo_servicio = tipo_servicio;
    }

    public String getTipo_carro() {
        return tipo_carro;
    }

    public void setTipo_carro(String tipo_carro) {
        this.tipo_carro = tipo_carro;
    }

    public String getTipo_ser_costo() {
        return tipo_ser_costo;
    }

    public void setTipo_ser_costo(String tipo_ser_costo) {
        this.tipo_ser_costo = tipo_ser_costo;
    }

    @Override
    public String toString() {
        return "Servicio: " + tipo_servicio + "\nPara auto tipo: " + tipo_carro + "\nCosto: $" + tipo_ser_costo + ".00";
    }
}
