package com.bringsolutions.autolavadomovil.beta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bringsolutions.autolavadomovil.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class AdminBeta extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_beta);
        getSupportActionBar().setTitle("Menú Administrador");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    }

    public void irServicios(View view) {
        Intent menu = new Intent(AdminBeta.this, OpcionAdmin.class);
        menu.putExtra("opcion", "1");
        startActivity(menu);
    }

    public void irTiposCoches(View view) {
        Intent menu = new Intent(AdminBeta.this, OpcionAdmin.class);
        menu.putExtra("opcion", "2");
        startActivity(menu);
    }

    public void irGenerarOrden(View view) {
        Intent menu = new Intent(AdminBeta.this, OpcionAdmin.class);
        menu.putExtra("opcion", "3");
        startActivity(menu);
    }

    public void irSolicitudes(View view) {
        startActivity(new Intent(AdminBeta.this, SolicitudesAdmin.class));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

}
