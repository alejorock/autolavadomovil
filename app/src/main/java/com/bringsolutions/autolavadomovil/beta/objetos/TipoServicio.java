package com.bringsolutions.autolavadomovil.beta.objetos;

public class TipoServicio {
    private String tipo_ser_id;
    private String tipo_servicio;
    private String tipo_ser_costo;

    public TipoServicio() {
    }

    public TipoServicio(String tipo_ser_id, String tipo_servicio, String tipo_ser_costo) {
        this.tipo_ser_id = tipo_ser_id;
        this.tipo_servicio = tipo_servicio;
        this.tipo_ser_costo = tipo_ser_costo;
    }

    public String getTipo_ser_id() {
        return tipo_ser_id;
    }

    public void setTipo_ser_id(String tipo_ser_id) {
        this.tipo_ser_id = tipo_ser_id;
    }

    public String getTipo_servicio() {
        return tipo_servicio;
    }

    public void setTipo_servicio(String tipo_servicio) {
        this.tipo_servicio = tipo_servicio;
    }

    public String getTipo_ser_costo() {
        return tipo_ser_costo;
    }

    public void setTipo_ser_costo(String tipo_ser_costo) {
        this.tipo_ser_costo = tipo_ser_costo;
    }

    @Override
    public String toString() {
        return tipo_servicio;
    }
}
