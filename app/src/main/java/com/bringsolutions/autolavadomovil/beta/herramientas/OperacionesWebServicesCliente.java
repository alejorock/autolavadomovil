package com.bringsolutions.autolavadomovil.beta.herramientas;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.autolavadomovil.beta.objetos.Constantes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OperacionesWebServicesCliente {
      private static String idUltimoServicioInsertado;
    private static String idClaveOrdenInsertada = "";

    public static final String registrarSolicitudServicio(final Context context, String direccion, String numero, String nombre) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = Constantes.URL_PRINCIPAL + "insertar_solicitud.php?direccion=" + direccion + "&contacto=" + numero + "&responsable=" + nombre;
        url = url.replace(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray nodoIDinsertado = jsonObject.getJSONArray("id_insertado");
                    Toast.makeText(context, "¡Tu solicitud ha generado correctamente!", Toast.LENGTH_SHORT).show();
                   Constantes.ID_SERVICIO_INSERTADO = nodoIDinsertado.getJSONObject(0).getString("id_insertado_servicio");
                } catch (JSONException e) {
                    Toast.makeText(context, "Hubo un problema al intentar generar su solcitud", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Hubo un problema al intentar generar su solcitud", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);


        return null;
    }


    public static String registrarOrden(final Context context, String idServicioInsertado, final String claveOrden) {
        idClaveOrdenInsertada = "";
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = Constantes.URL_PRINCIPAL + "insertar_orden.php?claveorden=" + claveOrden + "&id_solicitud=" + idServicioInsertado;
        url = url.replace(" ", "%20");
        System.out.println("ÑAA UEE S: " + url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray nodoIDinsertado = jsonObject.getJSONArray("id_orden_insertada");
                    idClaveOrdenInsertada = nodoIDinsertado.getJSONObject(0).getString("id_insertado_orden");

                    //   Toast.makeText(context, "¡Tu orden " + claveOrden + " se ha generado correctamente!", Toast.LENGTH_SHORT).show();

                    System.out.println("TRAER IIDD orden:" + idClaveOrdenInsertada);

                } catch (JSONException e) {
                    System.out.println("EEROR ORDEN 1:" + e.toString());
                    Toast.makeText(context, "Hubo un problema al intentar generar su orden", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("EEROR ORDEN 2:" + error.toString());

                Toast.makeText(context, "Hubo un problema al intentar generar su orden", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
        return idClaveOrdenInsertada;
    }


    public static String generarOrdenServicio(String idSolicitudInsertada) {
        String res = "";


        return res;
    }

}
