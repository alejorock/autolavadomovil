package com.bringsolutions.autolavadomovil.beta.adaptadores.admin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.autolavadomovil.R;
import com.bringsolutions.autolavadomovil.beta.objetos.Constantes;
import com.bringsolutions.autolavadomovil.beta.objetos.OrdenServicio;
import com.bringsolutions.autolavadomovil.beta.objetos.ServicioOrdenClave;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OrdenesServicioAdaptador extends RecyclerView.Adapter<OrdenesServicioAdaptador.ViewHolder> {
    AlertDialog dialog;
    List<ServicioOrdenClave> SERVICIOS_X_ORDEN_LISTA = new ArrayList<>();
    float sumaSubTotal = 0;
    TextView tvMontoTotalInformacionOrden, tvInformacionEstatusOrden;
    Switch swActualizarEstatusInformacionOrden, swFinalizarOrdenInformacionOrden;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNumOrden, tvDireccionOrden, tvEstatusOrden;
        private CardView tarjetaOrden;
        private ImageView btnMostrarDetallesOrden;
        private LinearLayout lnBackgroudEstatus;

        public ViewHolder(View itemView) {
            super(itemView);

            tvNumOrden = itemView.findViewById(R.id.tvClaveOrden);
            tvDireccionOrden = itemView.findViewById(R.id.tvDireccionOrden);
            tvEstatusOrden = itemView.findViewById(R.id.tvEstatusOrdenServicio);
            tarjetaOrden = itemView.findViewById(R.id.tarjetaOrdenServicio);
            btnMostrarDetallesOrden = itemView.findViewById(R.id.btnDetalleOrden);
            lnBackgroudEstatus = itemView.findViewById(R.id.linearBackgroundEstatusOrden);

        }
    }


    public List<OrdenServicio> ordenServicioList;
    public Context context;

    public OrdenesServicioAdaptador(List<OrdenServicio> ordenServicioList, Context context) {
        this.ordenServicioList = ordenServicioList;
        this.context = context;
    }

    @NonNull
    @Override
    public OrdenesServicioAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_orden_servicio, viewGroup, false);
        return new OrdenesServicioAdaptador.ViewHolder(view);
    }

    public void onBindViewHolder(@NonNull final OrdenesServicioAdaptador.ViewHolder viewHolder, final int i) {
        viewHolder.tvNumOrden.setText(ordenServicioList.get(i).getOrd_clave());
        viewHolder.tvDireccionOrden.setText(ordenServicioList.get(i).getSol_direccion());

        if (ordenServicioList.get(i).getEstatus().equals("0")) {
            viewHolder.tvEstatusOrden.setText("En espera...");
            viewHolder.lnBackgroudEstatus.setBackgroundColor(Color.rgb(49, 49, 49));
        } else if (ordenServicioList.get(i).getEstatus().equals("1")) {
            viewHolder.tvEstatusOrden.setText("En camino...");
            viewHolder.lnBackgroudEstatus.setBackgroundColor(Color.rgb(57, 0, 138));
        } else if (ordenServicioList.get(i).getEstatus().equals("2")) {
            viewHolder.tvEstatusOrden.setText("¡Orden Finalizada!");
            viewHolder.lnBackgroudEstatus.setBackgroundColor(Color.rgb(78, 147, 117));
        }

        viewHolder.btnMostrarDetallesOrden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanzarDialogoInformacionOrdenServicio(ordenServicioList.get(i).getOrd_clave(), ordenServicioList.get(i).getSol_direccion(), ordenServicioList.get(i).getSol_responsable(), ordenServicioList.get(i).getSol_contacto(), viewHolder.lnBackgroudEstatus, viewHolder.tvEstatusOrden, ordenServicioList.get(i).getEstatus(), i);
            }
        });

        viewHolder.tarjetaOrden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanzarDialogoInformacionOrdenServicio(ordenServicioList.get(i).getOrd_clave(), ordenServicioList.get(i).getSol_direccion(), ordenServicioList.get(i).getSol_responsable(), ordenServicioList.get(i).getSol_contacto(), viewHolder.lnBackgroudEstatus, viewHolder.tvEstatusOrden, ordenServicioList.get(i).getEstatus(), i);

            }
        });


    }

    private void lanzarDialogoInformacionOrdenServicio(final String ord_clave, String sol_direccion, String sol_responsable, final String sol_contacto, final LinearLayout lnBackgroudEstatus, final TextView tvEstatusOrden, final String estatus, final int posicion) {
        sumaSubTotal = 0;
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewDialog = layoutInflater.inflate(R.layout.dialogo_informacion_orden_servicio_admin, null);

        builder.setView(viewDialog);

        final TextView tvClaveOrden, tvResponsable, tvDireccion, tvTelefono, tvInformacionFinalizacionOrden;
        final ListView listview;
        final Button btnContactarPersona;

        tvClaveOrden = viewDialog.findViewById(R.id.tvInformacionOrdenCalve);
        listview = viewDialog.findViewById(R.id.listaDialogoServiciosOrden);
        tvResponsable = viewDialog.findViewById(R.id.tvNombreResponsableInformacionOrden);
        tvDireccion = viewDialog.findViewById(R.id.tvDireccionDialogoInformacionOrden);
        tvTelefono = viewDialog.findViewById(R.id.tvTelefonoDialogoInformacionOrden);
        btnContactarPersona = viewDialog.findViewById(R.id.btnContactarDialogoInformacionOrden);

        tvMontoTotalInformacionOrden = viewDialog.findViewById(R.id.tvMontoTotalInformacionOrden);
        tvInformacionEstatusOrden = viewDialog.findViewById(R.id.tvInformacionEstatusOrden);
        tvInformacionFinalizacionOrden = viewDialog.findViewById(R.id.tvInformacionFinalizacionOrden);
        swActualizarEstatusInformacionOrden = viewDialog.findViewById(R.id.swActualizarEstatusInformacionOrden);
        swFinalizarOrdenInformacionOrden = viewDialog.findViewById(R.id.swFinalizarOrdenInformacionOrden);

        if (estatus.equals("0")) {
            swActualizarEstatusInformacionOrden.setChecked(false);
            tvInformacionEstatusOrden.setText("En espera...");
            lnBackgroudEstatus.setBackgroundColor(Color.rgb(49, 49, 49));

        } else if (estatus.equals("1")) {
            swActualizarEstatusInformacionOrden.setChecked(true);
            tvInformacionEstatusOrden.setText("En camino...");
            lnBackgroudEstatus.setBackgroundColor(Color.rgb(57, 0, 138));
        } else if (estatus.equals("2")) {
            swActualizarEstatusInformacionOrden.setChecked(true);
            swFinalizarOrdenInformacionOrden.setChecked(true);
            tvInformacionEstatusOrden.setText("¡Orden Finalizada!");
            tvInformacionFinalizacionOrden.setText("¡Orden Finalizada!");
            tvEstatusOrden.setText("¡Orden Finalizada!");
            lnBackgroudEstatus.setBackgroundColor(Color.rgb(78, 147, 117));
        }

        tvClaveOrden.setText(ord_clave);
        tvDireccion.setText(sol_direccion);
        tvResponsable.setText(sol_responsable);
        tvTelefono.setText(sol_contacto);

        poblarListaServiciosOrden(ord_clave, listview);

        btnContactarPersona.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sol_contacto)));
            }
        });

        swActualizarEstatusInformacionOrden.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (swFinalizarOrdenInformacionOrden.isChecked() == false) {
                        actualizarEstatusOrden(context, "1", ord_clave);
                    }
                    tvInformacionEstatusOrden.setText(swFinalizarOrdenInformacionOrden.isChecked() ? "¡Orden Finalizada!" : "En camino...");
                    tvEstatusOrden.setText(swFinalizarOrdenInformacionOrden.isChecked() ? "¡Orden Finalizada!" : "En camino...");
                    lnBackgroudEstatus.setBackgroundColor(swFinalizarOrdenInformacionOrden.isChecked() ? Color.rgb(78, 147, 117) : Color.rgb(57, 0, 138));
                    ordenServicioList.get(posicion).setEstatus(swFinalizarOrdenInformacionOrden.isChecked() ? "2" : "1");

                } else {
                    actualizarEstatusOrden(context, "0", ord_clave);
                    tvInformacionEstatusOrden.setText("En espera...");
                    tvEstatusOrden.setText("En espera...");
                    lnBackgroudEstatus.setBackgroundColor(Color.rgb(49, 49, 49));
                    swFinalizarOrdenInformacionOrden.setChecked(false);
                    tvInformacionFinalizacionOrden.setText("Aún no se ha finalizado");
                    ordenServicioList.get(posicion).setEstatus("0");
                }
            }
        });

        swFinalizarOrdenInformacionOrden.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    actualizarEstatusOrden(context, "2", ord_clave);
                    tvInformacionEstatusOrden.setText("¡Orden Finalizada!");
                    tvEstatusOrden.setText("¡Orden Finalizada!");
                    lnBackgroudEstatus.setBackgroundColor(Color.rgb(78, 147, 117));
                    tvInformacionFinalizacionOrden.setText("¡Orden Finalizada!");
                    swActualizarEstatusInformacionOrden.setChecked(true);
                    ordenServicioList.get(posicion).setEstatus("2");

                } else {
                    actualizarEstatusOrden(context, "0", ord_clave);
                    ordenServicioList.get(posicion).setEstatus("0");
                    swActualizarEstatusInformacionOrden.setChecked(false);
                    tvInformacionEstatusOrden.setText("En espera...");
                    tvEstatusOrden.setText("En espera...");
                    lnBackgroudEstatus.setBackgroundColor(Color.rgb(49, 49, 49));
                }
            }
        });

        dialog = builder.create();

        dialog.show();
    }

    private void actualizarEstatusOrden(final Context context, String estatus, String ord_clave) {
        final RequestQueue requestQueue = Volley.newRequestQueue(context);

        String url = Constantes.URL_PRINCIPAL + "actualizar_estatus_orden.php?estatus=" + estatus + "&clave_orden=" + ord_clave;

        url = url.replace(" ", "%20");
        StringRequest stringRequest = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(context, "Estatus actualizado", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        requestQueue.add(stringRequest);


    }

    public int getItemCount() {
        return ordenServicioList.size();
    }


    public void poblarListaServiciosOrden(String claveOrden, final ListView listview) {
        SERVICIOS_X_ORDEN_LISTA.clear();
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        String url = Constantes.URL_PRINCIPAL + "obtener_ordenes_servicio_x_clave_orden.php?clave_orden=" + claveOrden;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray arrayCapacitaciones = response.getJSONArray("servicios_orden");
                    JSONObject jsonObject = null;
                    for (int i = 0; i < arrayCapacitaciones.length(); i++) {
                        jsonObject = arrayCapacitaciones.getJSONObject(i);
                        String tipo_servicio = jsonObject.optString("tipo_servicio");
                        String tipo_carro = jsonObject.optString("tipo_carro");
                        String tipo_ser_costo = jsonObject.optString("tipo_ser_costo");

                        SERVICIOS_X_ORDEN_LISTA.add(new ServicioOrdenClave(tipo_servicio, tipo_carro, tipo_ser_costo));
                        sumaSubTotal = sumaSubTotal + Float.parseFloat(tipo_ser_costo);
                        tvMontoTotalInformacionOrden.setText(String.valueOf(sumaSubTotal));

                    }

                    final ArrayAdapter<ServicioOrdenClave> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, SERVICIOS_X_ORDEN_LISTA);
                    listview.setAdapter(adapter);

                } catch (Exception e) {
                    System.out.println("EL error es: " + e.toString());

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("EL error es: " + error.toString());

            }

        });

        requestQueue.add(jsonObjectRequest);

    }

}
