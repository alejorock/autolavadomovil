package com.bringsolutions.autolavadomovil.beta.objetos;

public class Servicio {
    private String ser_id;
    private String ord_id;
    private String tipo_car_id;
    private String tipo_ser_id;

    public Servicio(Object selectedItem) {
    }

    public Servicio(String ser_id, String ord_id, String tipo_car_id, String tipo_ser_id) {
        this.ser_id = ser_id;
        this.ord_id = ord_id;
        this.tipo_car_id = tipo_car_id;
        this.tipo_ser_id = tipo_ser_id;
    }

    public Servicio( String ord_id, String tipo_car_id, String tipo_ser_id) {
        this.ord_id = ord_id;
        this.tipo_car_id = tipo_car_id;
        this.tipo_ser_id = tipo_ser_id;
    }

    public String getSer_id() {
        return ser_id;
    }

    public void setSer_id(String ser_id) {
        this.ser_id = ser_id;
    }

    public String getOrd_id() {
        return ord_id;
    }

    public void setOrd_id(String ord_id) {
        this.ord_id = ord_id;
    }

    public String getTipo_car_id() {
        return tipo_car_id;
    }

    public void setTipo_car_id(String tipo_car_id) {
        this.tipo_car_id = tipo_car_id;
    }

    public String getTipo_ser_id() {
        return tipo_ser_id;
    }

    public void setTipo_ser_id(String tipo_ser_id) {
        this.tipo_ser_id = tipo_ser_id;
    }

    @Override
    public String toString() {
        return "Servicio{" +
                "ser_id='" + ser_id + '\'' +
                ", ord_id='" + ord_id + '\'' +
                ", tipo_car_id='" + tipo_car_id + '\'' +
                ", tipo_ser_id='" + tipo_ser_id + '\'' +
                '}';
    }


}
