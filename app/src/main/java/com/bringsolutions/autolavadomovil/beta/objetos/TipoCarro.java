package com.bringsolutions.autolavadomovil.beta.objetos;

public class TipoCarro {
    private String tipo_carro_id;
    private String tipo_carro;

    public TipoCarro() {
    }

    public TipoCarro(String tipo_carro_id, String tipo_carro) {
        this.tipo_carro_id = tipo_carro_id;
        this.tipo_carro = tipo_carro;
    }

    public String getTipo_carro_id() {
        return tipo_carro_id;
    }

    public void setTipo_carro_id(String tipo_carro_id) {
        this.tipo_carro_id = tipo_carro_id;
    }

    public String getTipo_carro() {
        return tipo_carro;
    }

    public void setTipo_carro(String tipo_carro) {
        this.tipo_carro = tipo_carro;
    }
}
