package com.bringsolutions.autolavadomovil.beta.objetos;

import java.io.Serializable;

public class DescripcionServicio implements Serializable {
    private Servicio servicio;
    private String nombreServicio;
    private String tipoCoche;
    private String costoServicio;

    public DescripcionServicio(Servicio servicio, String nombreServicio, String tipoCoche, String costoServicio) {
        this.servicio = servicio;
        this.nombreServicio = nombreServicio;
        this.tipoCoche = tipoCoche;
        this.costoServicio = costoServicio;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public String getNombreServicio() {
        return nombreServicio;
    }

    public void setNombreServicio(String nombreServicio) {
        this.nombreServicio = nombreServicio;
    }

    public String getTipoCoche() {
        return tipoCoche;
    }

    public void setTipoCoche(String tipoCoche) {
        this.tipoCoche = tipoCoche;
    }

    public String getCostoServicio() {
        return costoServicio;
    }

    public void setCostoServicio(String costoServicio) {
        this.costoServicio = costoServicio;
    }

    @Override
    public String toString() {
        return "Servicio: " +nombreServicio+"\nPara auto tipo: " +tipoCoche+"\nCosto: $" +costoServicio+".00";
    }
}
