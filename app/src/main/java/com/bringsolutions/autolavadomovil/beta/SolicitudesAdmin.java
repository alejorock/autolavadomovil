package com.bringsolutions.autolavadomovil.beta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.autolavadomovil.R;
import com.bringsolutions.autolavadomovil.beta.adaptadores.admin.OrdenesServicioAdaptador;
import com.bringsolutions.autolavadomovil.beta.objetos.Constantes;
import com.bringsolutions.autolavadomovil.beta.objetos.OrdenServicio;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SolicitudesAdmin extends AppCompatActivity {
    RecyclerView recyclerOrdenes;
    private OrdenesServicioAdaptador ordenesServicioAdaptador;
    List<OrdenServicio> ORDENES_SERVICIO_LISTA = new ArrayList<>();
    CardView btnEspera, btnCamino, btnFinalizadas;
    TextView tvEspera, tvCamino, tvFinalizadas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitudes_admin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        inicializarElementos();


    }

    private void inicializarElementos() {
        recyclerOrdenes = findViewById(R.id.recyclerOrdenesServicio);

        recyclerOrdenes.setLayoutManager(new LinearLayoutManager(SolicitudesAdmin.this));
        findViewById(R.id.lnCargandoOrdenesServicio).setVisibility(View.VISIBLE);
        btnEspera = findViewById(R.id.btnEsperaOrdenes);
        btnCamino = findViewById(R.id.btnCaminoOrdenes);
        btnFinalizadas = findViewById(R.id.btnFinalizadas);
        tvEspera = findViewById(R.id.tvEnEsperaSolicitudes);
        tvCamino = findViewById(R.id.tvEnCaminoSolicitudes);
        tvFinalizadas = findViewById(R.id.tvFinalizadasSolicitudes);
        clicks();

        poblarRecyler(3);


    }

    private void clicks() {
        btnEspera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnEspera.setCardBackgroundColor(Color.rgb(49, 49, 49));
                tvEspera.setTextColor(Color.rgb(255, 255, 255));


                tvCamino.setTextColor(Color.rgb(130, 130, 130));
                tvFinalizadas.setTextColor(Color.rgb(130, 130, 130));
                btnCamino.setCardBackgroundColor(Color.rgb(255, 255, 255));
                btnFinalizadas.setCardBackgroundColor(Color.rgb(255, 255, 255));

                try {
                    poblarRecyler(0);
                } catch (Exception e) {

                }
            }
        });

        btnCamino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCamino.setCardBackgroundColor(Color.rgb(57, 0, 138));
                tvCamino.setTextColor(Color.rgb(255, 255, 255));

                tvFinalizadas.setTextColor(Color.rgb(130, 130, 130));
                tvEspera.setTextColor(Color.rgb(130, 130, 130));
                btnEspera.setCardBackgroundColor(Color.rgb(255, 255, 255));
                btnFinalizadas.setCardBackgroundColor(Color.rgb(255, 255, 255));
                try {
                    poblarRecyler(1);
                } catch (Exception e) {

                }
            }
        });

        btnFinalizadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnFinalizadas.setCardBackgroundColor(Color.rgb(78, 147, 117));
                tvFinalizadas.setTextColor(Color.rgb(255, 255, 255));

                btnEspera.setCardBackgroundColor(Color.rgb(255, 255, 255));
                tvEspera.setTextColor(Color.rgb(130, 130, 130));
                tvCamino.setTextColor(Color.rgb(130, 130, 130));
                btnCamino.setCardBackgroundColor(Color.rgb(255, 255, 255));
                try {
                    poblarRecyler(2);
                } catch (Exception e) {

                }
            }
        });

    }

    private void poblarRecyler(final int tipoFiltro) {
        findViewById(R.id.lnContenedorSolicitudes).setVisibility(View.GONE);

        findViewById(R.id.lnCargandoOrdenesServicio).setVisibility(View.VISIBLE);

        ORDENES_SERVICIO_LISTA.clear();
        recyclerOrdenes.clearAnimation();
        RequestQueue requestQueue = Volley.newRequestQueue(SolicitudesAdmin.this);

        String url = Constantes.URL_PRINCIPAL + "obtener_ordenes_servicio.php";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray arrayCapacitaciones = response.getJSONArray("ordenes_servicio");
                    JSONObject jsonObject = null;

                    for (int i = 0; i < arrayCapacitaciones.length(); i++) {
                        jsonObject = arrayCapacitaciones.getJSONObject(i);


                        String ord_clave = jsonObject.optString("ord_clave");
                        String estatus = jsonObject.optString("estatus");
                        String sol_direccion = jsonObject.optString("sol_direccion");
                        String sol_responsable = jsonObject.optString("sol_responsable");
                        String sol_contacto = jsonObject.optString("sol_contacto");

                        if (tipoFiltro == 0 && Integer.parseInt(estatus) == 0) {
                            ORDENES_SERVICIO_LISTA.add(new OrdenServicio(ord_clave, estatus, sol_direccion, sol_responsable, sol_contacto));

                        } else if (tipoFiltro == 1 && Integer.parseInt(estatus) == 1) {
                            ORDENES_SERVICIO_LISTA.add(new OrdenServicio(ord_clave, estatus, sol_direccion, sol_responsable, sol_contacto));

                        } else if (tipoFiltro == 2 && Integer.parseInt(estatus) == 2) {
                            ORDENES_SERVICIO_LISTA.add(new OrdenServicio(ord_clave, estatus, sol_direccion, sol_responsable, sol_contacto));

                        } else if (tipoFiltro == 3) {
                            ORDENES_SERVICIO_LISTA.add(new OrdenServicio(ord_clave, estatus, sol_direccion, sol_responsable, sol_contacto));

                        }

                    }

                    ordenesServicioAdaptador = new OrdenesServicioAdaptador(ORDENES_SERVICIO_LISTA, SolicitudesAdmin.this);
                    recyclerOrdenes.setAdapter(ordenesServicioAdaptador);
                    ordenesServicioAdaptador.notifyDataSetChanged();

                    findViewById(R.id.lnCargandoOrdenesServicio).setVisibility(View.GONE);
                    findViewById(R.id.lnContenedorSolicitudes).setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    System.out.println("EL error es: " + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override


            public void onErrorResponse(VolleyError error) {
                System.out.println("EL error es: " + error.toString());

            }


        });

        requestQueue.add(jsonObjectRequest);


    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }
}
