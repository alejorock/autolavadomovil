package com.bringsolutions.autolavadomovil.beta;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;

import com.bringsolutions.autolavadomovil.R;
import com.bringsolutions.autolavadomovil.beta.fragments.admin.GenerarOrden;
import com.bringsolutions.autolavadomovil.beta.fragments.admin.TiposServicios;

public class OpcionAdmin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opcion_admin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String opcionElegida = intent.getStringExtra("opcion");

        FragmentManager fm = getFragmentManager();

        switch (opcionElegida) {
            case "1":
                fm.beginTransaction().replace(R.id.contenedorOpcionesAdmin, new TiposServicios()).addToBackStack(null).commit();

                break;

            case "2":

                break;

            case "3":
                fm.beginTransaction().replace(R.id.contenedorOpcionesAdmin, new GenerarOrden()).addToBackStack(null).commit();

                break;
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }
}
