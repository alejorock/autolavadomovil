package com.bringsolutions.autolavadomovil.beta.fragments.admin;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import android.app.Fragment;

import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.autolavadomovil.R;
import com.bringsolutions.autolavadomovil.beta.objetos.Constantes;
import com.bringsolutions.autolavadomovil.beta.objetos.DescripcionServicio;
import com.bringsolutions.autolavadomovil.beta.objetos.Servicio;
import com.bringsolutions.autolavadomovil.beta.objetos.TipoServicio;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class GenerarOrden extends Fragment {

    View view;
    //VARIABLES PARA LAS ORDENES DE SERVICIO
    String idClaveOrdenInsertada = "";
    float costoServicioAux = 0, sumaSubTotalPrincipal = 0;

    CardView btnGenerarOrdenServicio;
    AlertDialog dialogoPrincipalSolicitudServicio;

    List<TipoServicio> TIPOS_SERVICIOS_LISTA = new ArrayList<>();
    List<DescripcionServicio> LISTA_SERVICIOS_DESCIPCION = new ArrayList<>();
    Spinner spnServicios;
    TextView tvPrecioServicio;
    List<Servicio> SERVICIOS_LISTA = new ArrayList<>();
    int tipoCocheSelecionado = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_generar_orden, container, false);
        inicializarElementos();
        clicks();


        return view;
    }

    private void clicks() {
        btnGenerarOrdenServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoSolicitud();
            }
        });
    }

    private void dialogoSolicitud() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();

        final View viewDialog = layoutInflater.inflate(R.layout.dialogo_solicitud, null);

        builder.setView(viewDialog);

        final CardView btnServicioDireccionGenerada, btnServicioDireccionManual;
        final EditText cajaDireccionGenerada, cajaDireccionManual;
        final Button btnLinearPasoUno;
        final TextInputEditText cajaTelefono, cajaNombreSolicitante;

        btnServicioDireccionGenerada = viewDialog.findViewById(R.id.btnMiDireccionActual);
        btnServicioDireccionManual = viewDialog.findViewById(R.id.btnOtraDireccion);
        btnLinearPasoUno = viewDialog.findViewById(R.id.btnLinearPasoUnoSiguiente);
        cajaTelefono = viewDialog.findViewById(R.id.cajaTelefono);
        cajaNombreSolicitante = viewDialog.findViewById(R.id.cajaNombreSolicitante);
        final ImageButton imgButtonCerrarSolicitud = viewDialog.findViewById(R.id.imgButtonCerrarSolicitud);

        cajaDireccionGenerada = viewDialog.findViewById(R.id.cajaDireccionServicioGenerada);
        cajaDireccionManual = viewDialog.findViewById(R.id.cajaDireccionServicioManual);

        btnServicioDireccionGenerada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cajaDireccionGenerada.setVisibility(View.VISIBLE);
                cajaDireccionManual.setVisibility(View.GONE);

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                try {
                    LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    Geocoder geocoder;
                    List<Address> addresses;

                    geocoder = new Geocoder(getActivity(), Locale.getDefault());

                    addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 100);
                    String direccion = addresses.get(0).getAddressLine(0);
                    String codigopostal = addresses.get(0).getPostalCode();
                    if (codigopostal == null || codigopostal.equals("null") || codigopostal.isEmpty() || codigopostal == "") {
                        codigopostal = "SIN CP";
                    }
                    String direccionActual = direccion + ", " + codigopostal;
                    cajaDireccionGenerada.setText(direccionActual);


                } catch (Exception e) {
                }
            }
        });


        btnServicioDireccionManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cajaDireccionGenerada.setVisibility(View.GONE);
                cajaDireccionManual.setVisibility(View.VISIBLE);


            }
        });


        btnLinearPasoUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cajaNombreSolicitante.getText().toString().isEmpty() || cajaTelefono.getText().toString().isEmpty() || (cajaDireccionGenerada.getText().toString().isEmpty() && cajaDireccionManual.getText().toString().isEmpty())) {
                    Toast.makeText(getActivity(), "Algún campo sin llenarse", Toast.LENGTH_SHORT).show();
                } else {
                    viewDialog.findViewById(R.id.linearSolicitudPasoUno).setVisibility(View.GONE);
                    viewDialog.findViewById(R.id.linearSolicitudPasoDos).setVisibility(View.VISIBLE);
                    viewDialog.findViewById(R.id.linearPasoDosTab).setVisibility(View.VISIBLE);
                    viewDialog.findViewById(R.id.linearPasoDosContenedor).setVisibility(View.VISIBLE);
                    viewDialog.findViewById(R.id.linearPasoDosBotonSiguiente).setVisibility(View.VISIBLE);
                    viewDialog.findViewById(R.id.linearPasoDosOtroCocheMas).setVisibility(View.VISIBLE);
                    viewDialog.findViewById(R.id.linearPasoDosOpciones).setVisibility(View.VISIBLE);
                    registrarSolicitudServicio(cajaDireccionGenerada.getText().toString().isEmpty() ? cajaDireccionManual.getText().toString() : cajaDireccionGenerada.getText().toString(), cajaTelefono.getText().toString(), cajaNombreSolicitante.getText().toString());
                }
            }
        });

        final ImageView imgTipoCarro = viewDialog.findViewById(R.id.imgTipoDeCarroSolicitud);
        tvPrecioServicio = viewDialog.findViewById(R.id.tvCostoServicioSolicitud);
        final Button btnAgregarAutoOrden = viewDialog.findViewById(R.id.btnAgregarAutoOrden);
        spnServicios = viewDialog.findViewById(R.id.spnServicios);
        poblarSpinnerServicios();

        imgTipoCarro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tipoCocheSelecionado == 2) {
                    tipoCocheSelecionado = 1;
                    imgTipoCarro.setImageResource(R.drawable.ic_coche_chico);
                    Toast.makeText(getActivity(), "Coche", Toast.LENGTH_SHORT).show();

                } else if (tipoCocheSelecionado == 0) {
                    tipoCocheSelecionado = 1;
                    imgTipoCarro.setImageResource(R.drawable.ic_coche_chico);
                    Toast.makeText(getActivity(), "Coche", Toast.LENGTH_SHORT).show();


                } else if (tipoCocheSelecionado == 1) {
                    tipoCocheSelecionado = 2;
                    imgTipoCarro.setImageResource(R.drawable.ic_camioneta);
                    Toast.makeText(getActivity(), "Camioneta", Toast.LENGTH_SHORT).show();
                }
            }
        });


        spnServicios.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {

                int numTipoServicioSeleccionado = spnServicios.getSelectedItemPosition();
                TipoServicio tipoServicioSeleccionado = (TipoServicio) spnServicios.getItemAtPosition(numTipoServicioSeleccionado);
                tvPrecioServicio.setText(tipoServicioSeleccionado.getTipo_ser_costo());

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


        btnAgregarAutoOrden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tipoCocheSelecionado == 1 || tipoCocheSelecionado == 2) {
                    TipoServicio tipoServicio = (TipoServicio) spnServicios.getSelectedItem();
                    Servicio servicio = new Servicio(Constantes.CLAVE_ORDEN_GENERADA, String.valueOf(tipoCocheSelecionado), tipoServicio.getTipo_ser_id());
                    SERVICIOS_LISTA.add(servicio);

                    LISTA_SERVICIOS_DESCIPCION.add(new DescripcionServicio(servicio, tipoServicio.getTipo_servicio(), tipoCocheSelecionado == 1 ? "Coche" : "Camioneta", tipoServicio.getTipo_ser_costo()));

                    Toast.makeText(getActivity(), "Se ha añadido a tu orden " + Constantes.CLAVE_ORDEN_GENERADA, Toast.LENGTH_SHORT).show();
                    spnServicios.setSelection(0);
                    imgTipoCarro.setImageResource(R.drawable.ic_coche_lavado_ico);
                    tipoCocheSelecionado = 0;
                } else {
                    Toast.makeText(getActivity(), "Da un click en tipo de carro", Toast.LENGTH_SHORT).show();

                }

            }
        });

        Button btnPasoDosSiguiente = viewDialog.findViewById(R.id.btnPasoDosSiguiente);

        btnPasoDosSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanzarDialogoListaServiciosOrden();


            }
        });


        imgButtonCerrarSolicitud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoPrincipalSolicitudServicio.dismiss();
            }
        });



        dialogoPrincipalSolicitudServicio = builder.create();
        dialogoPrincipalSolicitudServicio.setCancelable(false);

        dialogoPrincipalSolicitudServicio.show();
    }

    private void poblarSpinnerServicios() {
        TIPOS_SERVICIOS_LISTA.clear();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        String url = "https://arigaleriadearte.com/api/autolavado/obtener_tipos_servicios.php";


        // crear un objeto requet
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                try {
                    JSONArray arrayCapacitaciones = response.getJSONArray("tipos_servicios");
                    JSONObject jsonObject = null;

                    for (int i = 0; i < arrayCapacitaciones.length(); i++) {
                        jsonObject = arrayCapacitaciones.getJSONObject(i);

                        String tipo_ser_id = jsonObject.optString("tipo_ser_id");
                        String tipo_servicio = jsonObject.optString("tipo_servicio");
                        String tipo_ser_costo = jsonObject.optString("tipo_ser_costo");

                        TIPOS_SERVICIOS_LISTA.add(new TipoServicio(tipo_ser_id, tipo_servicio, tipo_ser_costo));

                    }
                    ArrayAdapter<TipoServicio> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, TIPOS_SERVICIOS_LISTA);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spnServicios.setAdapter(adapter);


                } catch (Exception e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override


            public void onErrorResponse(VolleyError error) {
                System.out.println("EL error es: " + error.toString());

            }


        });

        requestQueue.add(jsonObjectRequest);


    }

    private void lanzarDialogoListaServiciosOrden() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialogo_lista_servicios_solicitados, null);

        builder.setView(viewDialog);

        final ListView listview;
        final TextView tvSubTotal;
        final Button btnAddServicio, btnAzulAgregarOtroServicioDialogo;
        final ArrayList<DescripcionServicio> servicioArrayList;
        listview = viewDialog.findViewById(R.id.listaServiciosOrden);
        btnAddServicio = viewDialog.findViewById(R.id.btnAddServicioDialogo);
        btnAzulAgregarOtroServicioDialogo = viewDialog.findViewById(R.id.btnAzulAgregarOtroServicioDialogo);

        final Button btnConfirmarEnviarDialogo = viewDialog.findViewById(R.id.btnConfirmarEnviarDialogo);

        tvSubTotal = viewDialog.findViewById(R.id.tvSubTotal);
        float sumaSubTotal = 0;

        servicioArrayList = new ArrayList<>();
        for (int i = 0; i < LISTA_SERVICIOS_DESCIPCION.size(); i++) {
            servicioArrayList.add(LISTA_SERVICIOS_DESCIPCION.get(i));
            sumaSubTotal = sumaSubTotal + Float.parseFloat(LISTA_SERVICIOS_DESCIPCION.get(i).getCostoServicio());
        }

        sumaSubTotalPrincipal = sumaSubTotal;
        tvSubTotal.setText(String.valueOf(sumaSubTotalPrincipal));
        final ArrayAdapter<DescripcionServicio> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, servicioArrayList);


        if (sumaSubTotalPrincipal == 0) {
            btnConfirmarEnviarDialogo.setVisibility(View.GONE);
            btnAzulAgregarOtroServicioDialogo.setVisibility(View.GONE);
            listview.setVisibility(View.GONE);
            viewDialog.findViewById(R.id.lnSinServicios).setVisibility(View.VISIBLE);
        }

        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                costoServicioAux = Float.parseFloat(servicioArrayList.get(position).getCostoServicio());
                sumaSubTotalPrincipal = sumaSubTotalPrincipal - costoServicioAux;

                SERVICIOS_LISTA.remove(servicioArrayList.get(position));
                LISTA_SERVICIOS_DESCIPCION.remove(servicioArrayList.get(position));
                adapter.remove(servicioArrayList.get(position));
                adapter.notifyDataSetChanged();

                if (sumaSubTotalPrincipal == 0) {
                    btnConfirmarEnviarDialogo.setVisibility(View.GONE);
                    btnAzulAgregarOtroServicioDialogo.setVisibility(View.GONE);
                    listview.setVisibility(View.GONE);
                    viewDialog.findViewById(R.id.lnSinServicios).setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), "¡Sin servicios!", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getActivity(), "¡Servicio eliminado!", Toast.LENGTH_SHORT).show();

                }

                tvSubTotal.setText(String.valueOf(sumaSubTotalPrincipal));

            }
        });


        final AlertDialog dialog = builder.create();
        dialog.setCancelable(false);

        btnAddServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnAzulAgregarOtroServicioDialogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnConfirmarEnviarDialogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarServiciosDeOrden(dialog);

            }
        });

        dialog.show();

    }

    public void enviarServiciosDeOrden(final AlertDialog dialogoListaServicios) {
        final RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setTitle("Enviar Ordenes de Servicio");
        builder1.setMessage("Su clave de orden es:  " + Constantes.CLAVE_ORDEN_GENERADA + " , con ella podrá seguir el estatus de su orden.");

        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "ACEPTAR",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            for (int i = 0; i < LISTA_SERVICIOS_DESCIPCION.size(); i++) {
                                String url = Constantes.URL_PRINCIPAL + "insertar_servicio_orden.php?orden_id=" + idClaveOrdenInsertada + "&tipo_car_id=" + LISTA_SERVICIOS_DESCIPCION.get(i).getServicio().getTipo_car_id() + "&tipo_ser_id=" + LISTA_SERVICIOS_DESCIPCION.get(i).getServicio().getTipo_ser_id();
                                url = url.replace(" ", "%20");


                                StringRequest stringRequest = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        /*ClipData clip = ClipData.newPlainText("text", "Texto copiado al portapapeles");
                                        ClipboardManager clipboard = (ClipboardManager)this.getSystemService(CLIPBOARD_SERVICE);
                                        clipboard.setPrimaryClip(clip);*/
                                        Toast.makeText(getActivity(), "¡Servicios enviados!", Toast.LENGTH_SHORT).show();
                                        dialogoListaServicios.dismiss();
                                        dialogoPrincipalSolicitudServicio.dismiss();
                                        LISTA_SERVICIOS_DESCIPCION.clear();
                                        SERVICIOS_LISTA.clear();
                                        sumaSubTotalPrincipal = 0;
                                        view.findViewById(R.id.lnServiciosEnviadosGenerarOrden).setVisibility(View.VISIBLE);

                                        new CountDownTimer(2500, 1000) {

                                            public void onTick(long millisUntilFinished) {
                                            }

                                            public void onFinish() {
                                                view.findViewById(R.id.lnServiciosEnviadosGenerarOrden).setVisibility(View.GONE);
                                            }

                                        }.start();
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                    }
                                });

                                requestQueue.add(stringRequest);
                            }

                        } catch (Exception e) {

                        }
                    }
                });

        builder1.setNegativeButton(
                "CANCELAR",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


    }




    //Este método genera una clave a través de la fecha actual y llama al método registrarOden
    public void generarClaveOrden(String idSolicitudInsertada) {
        Calendar fecha = Calendar.getInstance();
        int año = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH) + 1;
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        String claveOrden = "ORD-" + dia + "" + mes + idSolicitudInsertada;
        Constantes.CLAVE_ORDEN_GENERADA = claveOrden;
        registrarOrden(idSolicitudInsertada, claveOrden);
    }


    //Este método registra la orden con la idServicio y la clave de orden generada y guarda el valor en una variable global
    public void registrarOrden(String idServicioInsertado, String claveOrden) {
        final String claveOrdenRecibida = claveOrden;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = Constantes.URL_PRINCIPAL + "insertar_orden.php?claveorden=" + claveOrden + "&id_solicitud=" + idServicioInsertado;
        url = url.replace(" ", "%20");
        System.out.println("ÑAA UEE S: " + url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray nodoIDinsertado = jsonObject.getJSONArray("id_insertado_orden");
                    idClaveOrdenInsertada = nodoIDinsertado.getJSONObject(0).getString("id_orden_ultima");
                    Toast.makeText(getActivity(), "¡Tu orden " + claveOrdenRecibida + " se ha generado correctamente!", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    System.out.println("EEROR ORDEN 1:" + e.toString());
                    Toast.makeText(getActivity(), "Hubo un problema al intentar generar su orden", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("EEROR ORDEN 2:" + error.toString());
                Toast.makeText(getActivity(), "Hubo un problema al intentar generar su orden", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }


    //Este método registra la solicitud, obtiene la id del registro y por medio de ella, llama al método generar clave de orden
    public void registrarSolicitudServicio(String direccion, String numero, String nombre) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = Constantes.URL_PRINCIPAL + "insertar_solicitud.php?direccion=" + direccion + "&contacto=" + numero + "&responsable=" + nombre;
        url = url.replace(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray nodoIDinsertado = jsonObject.getJSONArray("id_insertado");
                    String idInsertado = nodoIDinsertado.getJSONObject(0).getString("id_insertado_servicio");

                    Toast.makeText(getActivity(), "Generando orden de servicio...", Toast.LENGTH_SHORT).show();

                    generarClaveOrden(idInsertado);
                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "Hubo un problema al intentar generar su solcitud", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Hubo un problema al intentar generar su solcitud", Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }



    private void inicializarElementos() {
        btnGenerarOrdenServicio = view.findViewById(R.id.btnGenerarOrdenAdmin);

    }


}
