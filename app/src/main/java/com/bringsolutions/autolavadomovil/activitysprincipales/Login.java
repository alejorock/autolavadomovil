package com.bringsolutions.autolavadomovil.activitysprincipales;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bringsolutions.autolavadomovil.R;
import com.bringsolutions.autolavadomovil.beta.AdminBeta;
import com.bringsolutions.autolavadomovil.beta.ClienteBeta;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;

public class Login extends AppCompatActivity {

    private Animation smalltobig, btta, btta2, atg, packageimg;
    private TextInputLayout cajaUser, cajaPass;
    private Button btnIniciarSesion;
    private ImageView imagenLogin;
    private TextView tvSaludo, tituloLogin, tituloRegistrarme;
    private TextView tvCreditos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        inicializarComponentes();


    }

    private void inicializarComponentes() {
        //Incializar animaciones
        smalltobig = AnimationUtils.loadAnimation(this, R.anim.smalltobig);
        btta = AnimationUtils.loadAnimation(this, R.anim.btta);
        btta2 = AnimationUtils.loadAnimation(this, R.anim.btta2);
        atg = AnimationUtils.loadAnimation(this, R.anim.atgthree);
        packageimg = AnimationUtils.loadAnimation(this, R.anim.packageimg);

        //Inicializar Componentes
        cajaUser = findViewById(R.id.cajaUser);
        cajaPass = findViewById(R.id.cajaPass);
        btnIniciarSesion = findViewById(R.id.btnIniciarSesion);
        tvSaludo = findViewById(R.id.tvSaludo);
        saludoDelDia(tvSaludo);
        imagenLogin = findViewById(R.id.imagenLogin);
        tituloLogin = findViewById(R.id.tituloLogin);
        tituloRegistrarme = findViewById(R.id.tituloRegistrarme);


        imagenLogin.startAnimation(smalltobig);
        tituloLogin.startAnimation(btta);
        tituloRegistrarme.startAnimation(atg);
        findViewById(R.id.tarjetaLogin).startAnimation(btta2);
        btnIniciarSesion.startAnimation(btta2);
        cajaUser.startAnimation(btta2);
        cajaPass.startAnimation(btta2);
        findViewById(R.id.btnSolicitarServicio).startAnimation(atg);
        tvSaludo.startAnimation(packageimg);
        tvCreditos = findViewById(R.id.tvCreditos);

        verificarPermisosGenerales();


        tvCreditos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Login.this, "Se abrirá el navegador...", Toast.LENGTH_SHORT).show();
                Uri uri = Uri.parse("http://www.vigux.com.mx/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });


    }

    public void sendViewClienteBeta(View view) {
        startActivity(new Intent(Login.this, ClienteBeta.class));
    }

    public void sendViewAdminBeta(View view) {
        try {
            String user = cajaUser.getEditText().getText().toString();
            String pass = cajaPass.getEditText().getText().toString();

            if (user.equals("admin") && pass.equals("administrador")) {
                startActivity(new Intent(Login.this, AdminBeta.class));
                Toast.makeText(this, "¡Bienvenido!", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(this, "¡Usuario incorrecto!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {

        }
    }

    public void sendViewRegistro(View view) {
        startActivity(new Intent(Login.this, Registro.class));
    }

    public static void saludoDelDia(TextView tvSaludo) {
        Calendar calendario = Calendar.getInstance();
        int horaDia = calendario.get(Calendar.HOUR_OF_DAY);
        if (horaDia >= 0 && horaDia < 12) {
            tvSaludo.setText("Buenos días!");
        } else if (horaDia >= 12 && horaDia < 15) {
            tvSaludo.setText("Buenas tardes!");
        } else if (horaDia >= 15 && horaDia < 19) {
            tvSaludo.setText("Buenas tardes!");
        } else if (horaDia >= 19 && horaDia < 24) {
            tvSaludo.setText("Buenas noches!");
        }
    }


    private void verificarPermisosGenerales() {
        int permisosRequestCode = 100;
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        int accessFinePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCoarsePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (accessFinePermission == PackageManager.PERMISSION_GRANTED && accessCoarsePermission == PackageManager.PERMISSION_GRANTED) {
            //se realiza metodo si es necesario...
        } else {
            ActivityCompat.requestPermissions(this, perms, permisosRequestCode);
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case 100:

                break;

        }
    }


}
