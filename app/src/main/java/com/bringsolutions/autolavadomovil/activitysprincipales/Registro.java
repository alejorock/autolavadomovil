package com.bringsolutions.autolavadomovil.activitysprincipales;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.bringsolutions.autolavadomovil.R;

public class Registro extends AppCompatActivity {
    private Animation izquierdaAnim, derechaAnim, arribaAnim, aparecerAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        getSupportActionBar().setTitle("Registrarse");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        inicializarElementos();


    }

    private void inicializarElementos() {
        izquierdaAnim = AnimationUtils.loadAnimation(this, R.anim.left_in);
        derechaAnim = AnimationUtils.loadAnimation(this, R.anim.btta);
        arribaAnim = AnimationUtils.loadAnimation(this, R.anim.smalltobig);
        aparecerAnim = AnimationUtils.loadAnimation(this, R.anim.fade_in);

        findViewById(R.id.tvTipoUsuario).startAnimation(arribaAnim);
        findViewById(R.id.linearCliente).startAnimation(izquierdaAnim);
        findViewById(R.id.linearServidor).startAnimation(derechaAnim);
        findViewById(R.id.linearForm).startAnimation(aparecerAnim);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

}
