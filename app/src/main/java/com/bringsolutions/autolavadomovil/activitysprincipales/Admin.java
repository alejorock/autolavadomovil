package com.bringsolutions.autolavadomovil.activitysprincipales;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.bringsolutions.autolavadomovil.R;

public class Admin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        getSupportActionBar().setTitle("Menú Administrador");

    }
}
