package com.bringsolutions.autolavadomovil.herramientas;

public class Solicitud {

    private String sol_id;
    private String sol_dir;
    private String sol_lati;
    private String sol_longi;
    private String sol_folio;
    private String sol_contacto;

    public Solicitud(String sol_id, String sol_dir, String sol_lati, String sol_longi, String sol_folio, String sol_contacto) {
        this.sol_id = sol_id;
        this.sol_dir = sol_dir;
        this.sol_lati = sol_lati;
        this.sol_longi = sol_longi;
        this.sol_folio = sol_folio;
        this.sol_contacto = sol_contacto;
    }

    //http://cruzrojatabasco.com/autolavws/insertar_solicitud.php?dir=Enrique%20Segoviano&lati=92.364696&longi=-32.3665666&contacto=9934341808


    public String getSol_id() {
        return sol_id;
    }

    public void setSol_id(String sol_id) {
        this.sol_id = sol_id;
    }

    public String getSol_dir() {
        return sol_dir;
    }

    public void setSol_dir(String sol_dir) {
        this.sol_dir = sol_dir;
    }

    public String getSol_lati() {
        return sol_lati;
    }

    public void setSol_lati(String sol_lati) {
        this.sol_lati = sol_lati;
    }

    public String getSol_longi() {
        return sol_longi;
    }

    public void setSol_longi(String sol_longi) {
        this.sol_longi = sol_longi;
    }

    public String getSol_folio() {
        return sol_folio;
    }

    public void setSol_folio(String sol_folio) {
        this.sol_folio = sol_folio;
    }

    public String getSol_contacto() {
        return sol_contacto;
    }

    public void setSol_contacto(String sol_contacto) {
        this.sol_contacto = sol_contacto;
    }
}
